package com.parseexcelsheet;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.yml")
@ConfigurationProperties
public class AppProperties {

    private String directory;
    private String input;
    private String output;
    private String lineTemplate;

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getLineTemplate() {
        return lineTemplate;
    }

    public void setLineTemplate(String lineTemplate) {
        this.lineTemplate = lineTemplate;
    }

    @Override
    public String toString() {
        return "AppProperties{" +
                "directory='" + directory + '\'' +
                ", input='" + input + '\'' +
                ", output='" + output + '\'' +
                ", lineTemplate='" + lineTemplate + '\'' +
                '}';
    }
}
