package com.parseexcelsheet;

import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ParseExcelSheet implements CommandLineRunner {

    // NOTE: see references in README.md

    @Resource
    private AppProperties appProperties;

    private static final Logger log = LoggerFactory.getLogger(ParseExcelSheet.class);

    public ParseExcelSheet(){}

    public ParseExcelSheet(String directory, String input, String output, String lineTemplate){
        logMissingProperties(directory, input, output, lineTemplate);
        if(StringUtils.isNotBlank(directory)){
            if(StringUtils.isNotBlank(input)){
                this.INPUT_FILE = directory + input;
            }
            if(StringUtils.isNotBlank(output)){
                this.OUTPUT_FILE = directory + output;
            }
        }
        if(StringUtils.isNotBlank(lineTemplate)){
            this.LINE_TEMPLATE = lineTemplate;
        }
    }

    private void logMissingProperties(String directory, String input, String output, String lineTemplate){
        if(StringUtils.isBlank(directory)){
            log.warn("property was blank: directory");
        }
        if(StringUtils.isBlank(input)){
            log.warn("property was blank: input");
        }
        if(StringUtils.isBlank(output)){
            log.warn("property was blank: output");
        }
        if(StringUtils.isBlank(lineTemplate)){
            log.warn("property was blank: line-template");
        }
    }

    String INPUT_FILE = "";
    String OUTPUT_FILE = "";
    String LINE_TEMPLATE = "";//"INSERT IGNORE INTO `log_mvr`.`transaction_log_attribute` (`transaction_id`,`attribute`,`sub_attribute`,`attribute_value`,`date_added`)VALUES(\"%s\",\"option\",\"END_OF_BILLING\",\"Y\",now());";

    public List<String> parseCsv() throws IOException {
        log.debug("Start parseCsv()");
        List<String> transactionIds = new ArrayList<>();
        Path path = Paths.get(INPUT_FILE);
        log.debug("input file was located ok in parseCsv()");
        try(Reader reader = Files.newBufferedReader(path)){
            try(CSVReader csvReader = new CSVReader(reader)){
                String[] line = null;
                boolean isTitleRow = true;
                while((line = csvReader.readNext()) != null){
                    // skip first row, since its titles only.
                    if(isTitleRow){
                        isTitleRow = false;
                        continue;
                    }
                    transactionIds.add(line[0]); // only use the first column, containing transaction id.
                }
            }
        }
        return transactionIds;
    }
    public Sheet openSheet() throws IOException {
        log.debug("Start openSheet()");
        FileInputStream fis = new FileInputStream(new File(INPUT_FILE));
        log.debug("input file was located ok in openSheet()");
        Workbook workbook = new XSSFWorkbook(fis);
        Sheet sheet = workbook.getSheetAt(0);
        return sheet;
    }

    public List<String> parseSheet() throws IOException {
        log.debug("Start parseSheet()");
        List<String> transactionIds = new ArrayList<String>();
        boolean isTitleRow = true;
        boolean isFirstColumn = true;
        Sheet sheet = openSheet();
        for(Row row : sheet){
            // skip first row, since its titles only.
            if(isTitleRow){
                isTitleRow = false;
                continue;
            }
            for(Cell cell : row){
                // only interested in the first column, which contains tID.
                if(isFirstColumn){
                    isFirstColumn = false;
                    CellType type = cell.getCellType();
                    if(type.equals(CellType.STRING)){
                        String value = cell.getRichStringCellValue().getString();
                        transactionIds.add(value);
                        //System.out.println("tid: " + value);
                    } else {
                        // skip this entry.
                        // tID will be a string. anything else should be avoided.
                    }
                } else {
                    // reset for next row
                    isFirstColumn = true;
                    break;
                }
            }
        }
        return transactionIds;
    }

    public void generateSqlFile(List<String> transactionIds) {
        log.debug("Start generateSqlFile()");
        PrintWriter printWriter = null;
        try {
            FileWriter fileWriter = new FileWriter(OUTPUT_FILE);
            log.debug("output file was located ok in generateSqlFile()");
            printWriter = new PrintWriter(fileWriter);
            int i = 1;
            for(String transactionId : transactionIds){
                // insert tID to output string, then write output to sql file.
                printWriter.printf(LINE_TEMPLATE, transactionId);
                // avoid println after final row.
                if(i++ < transactionIds.size()){
                    printWriter.println("");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(printWriter != null){
                printWriter.close();
            }
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(ParseExcelSheet.class, args);
        System.exit(0);
    }
    @Override
    public void run(String... args) {
        String directory = appProperties.getDirectory();
        String input = appProperties.getInput();
        String output = appProperties.getOutput();
        String lineTemplate = appProperties.getLineTemplate();
        ParseExcelSheet parseExcelSheet = new ParseExcelSheet(directory, input, output, lineTemplate);
        try {
            List<String> ids = null;
            if(input.endsWith(".csv")){
                ids = parseExcelSheet.parseCsv();
            } else {
                ids = parseExcelSheet.parseSheet();
            }
            parseExcelSheet.generateSqlFile(ids);
        } catch (IOException ioe) {
            log.error("Error processing a file. Check properties file, where directory and file names should be non-empty and correct, with file extensions.");
            ioe.printStackTrace();
        }
    }
}
