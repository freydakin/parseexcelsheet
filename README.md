# ParseExcelSheet

## Getting started

Get the code from GitLab, my personal repo.

https://gitlab.com/freydakin/parseexcelsheet

Find the blue "Clone" button, and select "Clone with HTTPS", or use this

https://gitlab.com/freydakin/parseexcelsheet.git

Navigate to the directory where your want the repo, and issue this command.

```
git clone https://gitlab.com/freydakin/parseexcelsheet.git
```

***

## Description of Code

This code uses the Apache POI library to read from an excel spreadsheet, 
or it uses the OpenCSV library to read from a csv file, to get 
transaction ids from it, and then to populate an output sql file with insert 
statements that contain the transaction ids.

application.yml contains the name of the input file (.xls, .csv), the name
of the output sql file (.sql), and the directory on your machine where both should
be located for this program to work. The directory can be either an absolute
address, anywhere on your machine, or in src/main/resources/ in the project.

The main program is the file ParseExcelSheet.java.

The other java file, AppProperties.java, is necessary for the springboot framework 
to allow access to the fields in the applications.yml file. With those fields, 
ParseExcelSheet.java is able to locate the input and output files.

## ParseExcelSheet

### Overview 

ParseExcelSheet is marked with @SpringBootApplication, making it a spring boot
application. It also implements CommandLineRunner, which allows it to override
the run() method. Since this is a springboot app, the main() method is used to 
start and stop the springboot app. The run() method is used to instantiate the 
ParseExcelSheet class, and populate it with values from AppProperties and 
application.yml

### Excel input files

For excel files the method "openSheet()" instantiates the Workbook. The method 
"parseSheet()" gets values from the first column of the spreadsheet, and contains 
transaction Ids from that first column, in an ArrayList. The method "generateSqlFile()" 
uses a PrintWriter to print sql insert statements to an output file. Each insert
statement contains one of the transaction ids that were collected in
"parseSheet()".

### CSV input files

For csv files, the method "parseCsv()" gets values from the first column of the
csv file, and stores those transaction ids in an ArrayList.
Then, the method "generateSqlFile()" operates exactly as it does for excel sheets.

## application.yml

application.yml contains 4 fields: input, output, directory, and line-template.

### input

The input is the name of the file to be parsed. It should be either a .csv file, or 
a .xlsx/xls file. This entry in the .yml file should contain the file extension,
in order for either parseSheet() or parseCsv() to be called, which the app determines
from the file extension. 

### output

The output can have any name, but since its contents are sql statements, the file extension 
here is .sql. This file extension allows it to be run as a sql script. 

### directory

The directory can be either an absolute address of a file location, if input and output files 
are located outside the project, or src/main/resources/ if you want to have input and output 
files contained in the resources directory of the project. You can use the existing files in 
the project for input and output, but for the input files you will need to replace their 
contents with your contents. And for the output file, its contents will be overwritten each 
time you run the program.

### Choices 

For input, output, and directory, the application.yml file is divided into
"Choice 1" and "Choice 2". Choice 1 is for files located outside of the project,
and Choice 2 is for files located in src/main/resources/. Uncomment the choice
that you wish to use, and comment out the choice that you dont wish to use.
Change the values as you see fit to make the app work for you.

### Line Template

The line-template is located in the application.yml file, but it doesnt need to be modified
unless you are changing the insert statement for some reason. This field is used along with
a printf() call, so the spot in this field where you see "%s" is where a transaction id
will be injected into the string, one for each line of the input.

## Running the App

The app was developed using Intellj IDEA Community Edition. This ide has a window
for "Maven", the Apache open source build program. When you make a change to the
code, when you are updating the app, or when you make changes to the file 
application.yml, you will want to rebuild the source code. To do this,
Open the "Maven" window, and find the "Lifecycle" folder. Open this folder,
and locate "clean" and "package". First double click "clean" to wipe out
any .jar artifacts that are left over from a previous build. Then double click
"package" for the artifacts to be packaged and ready to run.

To run the program, return to the "Maven" window, and locate the "Plugins" folder.
Open the sub-folder "spring-boot". The double click the command "spring-boot:run".
This will start springboot, and will lead in to running ParseExcelSheet.java.
Doing this is the same as navigating to the location of the java archive file in
the command line, and issuing the command "mvn spring-boot:run".

The program will run, you will see output and ascii art for springboot in the 
console. It will say something like:

"Started ParseExcelSheet in 3.087 seconds (JVM running for 6.752)"

to indicate that springboot has finished running. Then after running, the app
will exit, and you will see something like:

"Shutting down ExecutorService 'applicationTaskExecutor'"

"Process finished with exit code 0"

When this is done, check the output file (ie: output.sql) to see if insert statements
have been generated and populated in the file.

## References

[Parse Excel File via Java](https://www.baeldung.com/java-microsoft-excel)

[Parse CVS File via Java](https://www.baeldung.com/opencsv)

[Writing to file via Java](https://www.baeldung.com/java-write-to-file)

[Java MS Excel POI error](https://stackoverflow.com/questions/71970035/java-lang-nosuchmethoderror-org-apache-logging-log4j-logger-atdebug-with-poi)

[SpringBoot Basics](https://mkyong.com/spring-boot/spring-boot-hello-world-example-thymeleaf/)

[SpringBoot yaml files](https://mkyong.com/spring-boot/spring-boot-yaml-example/)

[SpringBoot Config Properties](https://mkyong.com/spring-boot/spring-boot-configurationproperties-example/)

***

## Authors
- Name: Steve Talbot
- Date: 5-10-2023
- Intended use: Support for Unbilled Transactions

## Project status
- Start Date: 5-10-2023
- Current Version: 1.0
- Current Status: Completed
